package org.usfirst.frc.team702.robot;

import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.SampleRobot;

import org.usfirst.frc.team702.pidtools.AdvancedPIDSource.PIDType;
import org.usfirst.frc.team702.sensors.PixyCmu5;
import org.usfirst.frc.team702.sensors.PixyCmu5.FrameOrder;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * This is a demo program showing the use of the RobotDrive class. The
 * SampleRobot class is the base of a robot application that will automatically
 * call your Autonomous and OperatorControl methods at the right time as
 * controlled by the switches on the driver station or the field controls.
 *
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the SampleRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 *
 * WARNING: While it may look like a good choice to use for your code if you're
 * inexperienced, don't. Unless you know what you are doing, complex code will
 * be much more difficult under this system. Use IterativeRobot or Command-Based
 * instead if you're new.
 */
public class Robot extends SampleRobot 	implements PIDOutput {
    RobotDrive myRobot;
    Joystick stick;
    PIDController turnController;
    double rotateToObjectRate;
	
	 /* The following PID Controller coefficients will need to be tuned */
    /* to match the dynamics of your drive system.  Note that the      */
    /* SmartDashboard in Test mode has support for helping you tune    */
    /* controllers by displaying a form where you can enter new P, I,  */
    /* and D constants and test the mechanism.                         */
    
    static final double kP = 0.03;
    static final double kI = 0.00;
    static final double kD = 0.00;
    static final double kF = 0.00;
    
    static final double kToleranceDegrees = 2.0f;

    // Channels for the wheels
    final static int frontLeftChannel	= 2;
    final static int rearLeftChannel	= 3;
    final static int frontRightChannel	= 1;
    final static int rearRightChannel	= 0;
	
	// Pixy Object
	PixyCmu5 pixycam;

	public Robot() {
		myRobot = new RobotDrive(frontLeftChannel, rearLeftChannel,
        		frontRightChannel, rearRightChannel);
        myRobot.setExpiration(0.1);
        stick = new Joystick(0);
       
    	try {
    		// Initialize Pixy Object to request data at a 50hz rate
    		pixycam = new PixyCmu5(new I2C(I2C.Port.kOnboard, 0x54), 0.02, null);
    		
    		// Set the PID Type to Angle
    		pixycam.setPIDType(PIDType.ANGLE);
    		
    		// Sort detections by Area
    		pixycam.setSortBy(FrameOrder.AREA);
    		
    		// Sort detections by largest Area
    		pixycam.setSortAscending(false);

    		/** 
    		 * Perform calculations when at least 1 object is detected 
    		 * This should be increased if the object you are tracking has multiple
    		 * individual color components that will be detected by the pixy. In this
    		 * case it will average the angle property of each element and return that.
    		 */
    		pixycam.setMinObjects(1);
    		
    	} catch (RuntimeException ex) {
    		DriverStation.reportError("Unable to Initialize Pixy: " + ex.getMessage(), true);
    	}
        
    	/**
    	 *  For a more customizable PID Source you can implement the PIDSourceWrapper
    	 *  interface using an anonymous class and use that when creating the
    	 *  turnController instance as shown here
    	 */
    	/*
    	   PIDSourceWrapper pixypidsource = new PIDSourceWrapper(pixycam) {
			
			@Override
			public double pidGet() {
				if(m_sensor instanceof PixyCmu5)
				{
					return ((PixyCmu5) m_sensor).getNormalizedOffset(2);
				} else {
					throw new RuntimeException("m_sensor must be of type PixyCmu5");
				}
			}
		};
		
		turnController = new PIDController(kP, kI, kD, kF, pixypidsource, this);
		*/
		
        turnController = new PIDController(kP, kI, kD, kF, pixycam, this);
        turnController.setInputRange(-180.0f,  180.0f);
        turnController.setOutputRange(-1.0, 1.0);
        turnController.setAbsoluteTolerance(kToleranceDegrees);
        turnController.setContinuous(true);
        
        /* Add the PID Controller to the Test-mode dashboard, allowing manual  */
        /* tuning of the Turn Controller's P, I and D coefficients.            */
        /* Typically, only the P value needs to be modified.                   */
        LiveWindow.addActuator("DriveSystem", "RotateController", turnController);
	}

    /**
     * Uses Mecanum drive and binds button 1 on the Joystick to enable turning to 
     * the largest object the pixy detects. Button 2 disables the turn controller
     */
    /* (non-Javadoc)
     * @see edu.wpi.first.wpilibj.SampleRobot#operatorControl()
     */
    public void operatorControl() {
        myRobot.setSafetyEnabled(true);
        boolean turnToObject = false;
        
        while (isOperatorControl() && isEnabled()) {
        	
        	double currentRotationRate;
        	
        	// Enable the turn controller
            if ( stick.getRawButton(1) && !turnToObject) {
            		turnController.setSetpoint(0.0f);
            		turnController.enable();
                	turnToObject = true;
            }
            
            // Disable and reset the turn controller
            if ( stick.getRawButton(2) && turnToObject) {
            	turnController.reset();
            	turnToObject = false;
            }
        
            
            /**
             * Set the rotation rate to use the calculated PID controller output
             * or the value from the stick  
             */
            if ( turnToObject ) {
                currentRotationRate = rotateToObjectRate;
            } else {
                currentRotationRate = stick.getTwist();
            }
            
            
            try {
                /**
                 *  Send the drive commands using the X and Y values from the stick and the rotation
                 *  rate from either the stick or the PID controller if enabled
                 */
                myRobot.mecanumDrive_Cartesian(stick.getX(), stick.getY(), currentRotationRate, 0);
                
            } catch( RuntimeException ex ) {
                DriverStation.reportError("Error communicating with drive system:  " + ex.getMessage(), true);
            }
            
            // Put some pixy data on the SmartDashboard
            SmartDashboard.putNumber("Pixy Objects Detected", pixycam.getNumObjectsDetected());
            SmartDashboard.putNumber("Pixy Object Angle from Center", pixycam.getTotalDegrees(1));
            SmartDashboard.putNumber("Pixy Object Age", pixycam.getDataAge());
            
            Timer.delay(0.005);	// Control the thread CPU usage
        }
    }

    /**
     * Runs during test mode
     */
    public void test() {
    }

    /**
     * This function implements the PID Output of the TurnController to 
     * write the current turn rate to be used in the robot drive
     */
    @Override
    public void pidWrite(double output) {
        rotateToObjectRate = output;
    }
}
