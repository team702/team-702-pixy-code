package org.usfirst.frc.team702.robot;

import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.SampleRobot;

import org.usfirst.frc.team702.pidtools.AdvancedPIDSource.PIDType;
import org.usfirst.frc.team702.sensors.PixyCmu5;
import org.usfirst.frc.team702.sensors.PixyCmu5.FrameOrder;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * This is a demo program showing the use of the RobotDrive class. The
 * SampleRobot class is the base of a robot application that will automatically
 * call your Autonomous and OperatorControl methods at the right time as
 * controlled by the switches on the driver station or the field controls.
 *
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the SampleRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 *
 * WARNING: While it may look like a good choice to use for your code if you're
 * inexperienced, don't. Unless you know what you are doing, complex code will
 * be much more difficult under this system. Use IterativeRobot or Command-Based
 * instead if you're new.
 */
public class Robot extends SampleRobot implements PIDOutput {
	RobotDrive myRobot;
	Joystick stick;
	PIDController strafeController;
	double strafeToObjectRate;
	
	/* The following PID Controller coefficients will need to be tuned */
    /* to match the dynamics of your drive system.  Note that the      */
    /* SmartDashboard in Test mode has support for helping you tune    */
    /* controllers by displaying a form where you can enter new P, I,  */
    /* and D constants and test the mechanism.                         */
    static final double kP = 2.00;
    static final double kI = 0.005;
    static final double kD = 0.00;
    static final double kF = 0.00;
    
    // The tolerance for strafe should be close to zero
    static final double kToleranceStrafe = 0.1f;

    // Channels for the wheels
    final static int frontLeftChannel	= 2;
    final static int rearLeftChannel	= 3;
    final static int frontRightChannel	= 1;
    final static int rearRightChannel	= 0;

    // The Pixy camera instance
    PixyCmu5 pixycam;

	public Robot() {
		myRobot = new RobotDrive(frontLeftChannel, rearLeftChannel,
        		frontRightChannel, rearRightChannel);
        myRobot.setExpiration(0.1);
        stick = new Joystick(0);
       
    	try {
    		// Initialize Pixy Object to request data at a 50hz rate
    		pixycam = new PixyCmu5(new I2C(I2C.Port.kOnboard, 0x54), 0.02, null);
    		
    		// Set the PID Type to Strafe
    		pixycam.setPIDType(PIDType.OFFSET);
    		
    		// Sort detections by X Center
    		pixycam.setSortBy(FrameOrder.XCenter);
    		
    		// Sort detections by farthest left object first
    		pixycam.setSortAscending(true);

    		/** 
    		 * Perform calculations when at least 2 objects are detected. This is set to 2
    		 * because it compares the relative aspect ratio of the object on the left
    		 * to the object on the right and normalizes it based on the total magnitude
    		 * of both areas. 
    		 */
    		pixycam.setMinObjects(2);
    		
    	} catch (RuntimeException ex) {
    		DriverStation.reportError("Unable to Initialize Pixy: " + ex.getMessage(), true);
    	}
    	strafeController = new PIDController(kP, kI, kD, kF, pixycam, this);
        strafeController.setInputRange(-1.0, 1.0);
        strafeController.setOutputRange(-1.0, 1.0);
        strafeController.setAbsoluteTolerance(kToleranceStrafe);
        strafeController.setContinuous(false);
        
        /* Add the PID Controller to the Test-mode dashboard, allowing manual  */
        /* tuning of the Turn Controller's P, I and D coefficients.            */
        /* Typically, only the P value needs to be modified.                   */
        LiveWindow.addActuator("DriveSystem", "StrafeController", strafeController);
	}

	/**
     * Uses Mecanum drive and binds button 1 on the Joystick to enable turning to 
     * the largest object the pixy detects. Button 2 disables the turn controller
     */
    /* (non-Javadoc)
     * @see edu.wpi.first.wpilibj.SampleRobot#operatorControl()
     */
	
	public void operatorControl() {
		 myRobot.setSafetyEnabled(true);
		 boolean strafeToObject = false;
		 
		 while (isOperatorControl() && isEnabled()) {
	        	
	        	double currentStrafeRate;
	        	
	        	// Enable the strafe controller
	            if (stick.getRawButton(1) && !strafeToObject) {
	            		strafeController.setSetpoint(0.0f);
	            		strafeController.enable();
	                	strafeToObject = true;
	            }
	            
	            // Disable and reset the strafe controller
	            if (stick.getRawButton(2) && strafeToObject) {
	            	strafeController.reset();
	            	strafeToObject = false;
	            }
	        
	            /**
	             * Set the strafe rate to use the calculated PID controller output
	             * or the value from the stick  
	             */
	            if (strafeToObject) {
	                currentStrafeRate = strafeToObjectRate;
	            } else {
	                currentStrafeRate = stick.getX();
	            }
	            
	            try {
	                /**
	                 *  Send the drive commands using the X and Y values from the stick and the rotation
	                 *  rate from either the stick or the PID controller if enabled
	                 */
	                myRobot.mecanumDrive_Cartesian(currentStrafeRate, stick.getY(), stick.getTwist(), 0);
	                
	            } catch( RuntimeException ex ) {
	                DriverStation.reportError("Error communicating with drive system:  " + ex.getMessage(), true);
	            }
	            
	            // Put some pixy data on the SmartDashboard
	            SmartDashboard.putNumber("Pixy Objects Detected", pixycam.getNumObjectsDetected());
	            SmartDashboard.putNumber("Pixy Object Angle from Center", pixycam.getTotalDegrees(1));
	            SmartDashboard.putNumber("Pixy Object Age", pixycam.getDataAge());
	            
	            Timer.delay(0.005);	// Control the thread CPU usage
	            
		 } // end while operatorControl
	} // end operatorControl
	
	public void test() {
		
	}
	
	/**
     * This function implements the PID Output of the TurnController to 
     * write the current turn rate to be used in the robot drive
     */
    @Override
    public void pidWrite(double output) {
        strafeToObjectRate = output;
    }
    
}